package com.daniweb.smartweather

object WeatherIconManager {
    private val CLOUDS = R.drawable.cloudy
    private val CLEAR = R.drawable.sun
    private val RAIN = R.drawable.rainy

    operator fun invoke(main: String): Int = when(main){
        "Clouds"-> CLOUDS
        "Rain" -> RAIN
        "Clear" -> CLEAR
        else -> CLEAR
    }
}