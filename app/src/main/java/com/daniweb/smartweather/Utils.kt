package com.daniweb.smartweather

import android.os.Build
import androidx.annotation.RequiresApi
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

internal object Utils {

    fun getDateTime(time: Long): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val date = Date(time * 1000)
        return sdf.format(date)
    }
    fun getDate(timestamp: Long): String {
        return SimpleDateFormat("EEEE dd MMM, yyyy", Locale.ENGLISH).format(timestamp * 1000)
    }

    fun getDayOfWeek(timestamp: Long): String {
        return SimpleDateFormat("EEE", Locale.ENGLISH).format(timestamp * 1000)
    }
}