/**
 * Copyright (C) Daniel Tovar Samano - All Rights Reserved *
 *  Unauthorized copying of this file, via any medium is strictly prohibited *
 *  Proprietary and confidential. *
 *  Written by Daniel Tovar Samano, Abril 2022 *
 */
package com.daniweb.smartweather.core

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

internal object RetrofitHelper {

    private val TIME_OUT_SECONDS = 10L
    /**
     * @brief Metodo para obetner un canal de comunicacion con el Host
     * @return A Retrofit object
     * */
    fun GetRetrofit(base_url: String): Retrofit {
        Log.d("RetrofitHelper", "RetrofitHelper, GetRetrofit")
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val gson = GsonBuilder().setLenient().create()

        val client: OkHttpClient.Builder = OkHttpClient.Builder()
        client.addInterceptor(loggingInterceptor)
        client.readTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS)
        return  Retrofit.Builder()
            .baseUrl(base_url)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client.build())
            .build()
    }

    fun GetRetrofit(base_url: String, timeUnit: Long): Retrofit {
        Log.d("RetrofitHelper", "RetrofitHelper, GetRetrofit")
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val gson = GsonBuilder().setLenient().create()

        val client: OkHttpClient.Builder = OkHttpClient.Builder()
        client.addInterceptor(loggingInterceptor)
        client.readTimeout(timeUnit, TimeUnit.SECONDS)
        return  Retrofit.Builder()
            .baseUrl(base_url)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client.build())
            .build()
    }

//    fun GetRetrofitString(base_url: String): Retrofit {
//        Log.d("RetrofitHelper", "RetrofitHelper, GetRetrofit")
//        val loggingInterceptor = HttpLoggingInterceptor()
//        loggingInterceptor.level = HttpLoggingInterceptor.Level.HEADERS
//
//        val client: OkHttpClient.Builder = OkHttpClient.Builder()
//        client.addInterceptor(loggingInterceptor)
//        client.readTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS)
//        return  Retrofit.Builder()
//            .baseUrl(base_url)
//            .addConverterFactory(ScalarsConverterFactory.create())
//            .client(client.build())
//            .build()
//    }


    /**
     * @brief Metodo para obetner un canal de comunicacion autenticado con el Host
     * @return A Retrofit object
     * */
    fun GetAuthRetrofit(base_url: String,user: String, password: String): Retrofit {
        Log.d("RetrofitHelper", "RetrofitHelper, GetAuthRetrofit")
        val authInterceptor = AuthInterceptor(user,password);
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val client: OkHttpClient.Builder = OkHttpClient.Builder()
        client.addInterceptor(authInterceptor)
        client.addInterceptor(loggingInterceptor)
        client.readTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS)

        return Retrofit.Builder()
            .baseUrl(base_url)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
    }

    /**
     * @brief Interceptor para agregar headers de Autorizacion
     * */
    private class AuthInterceptor (username: String, password: String) : Interceptor {
        private var credentials: String = Credentials.basic(username, password)
        override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
            Log.d("RetrofitHelper", "RetrofitHelper, intercept")
            var request = chain.request()
            request = request.newBuilder()
                .addHeader("Authorization", credentials)
                .build()
            return chain.proceed(request)
        }
    }
}