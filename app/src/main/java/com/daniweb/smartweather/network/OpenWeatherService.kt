package com.daniweb.smartweather.network

import OpenWeatherResponse
import com.daniweb.smartweather.core.RetrofitHelper
import com.daniweb.smartweather.network.models.WeatherResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

object OpenWeatherService {
    private val BASE_URL = "https://api.openweathermap.org/data/2.5/"

    private val provider: IOpenWeatherApi by lazy {
        RetrofitHelper.GetRetrofit(BASE_URL).create(IOpenWeatherApi::class.java)
    }

    suspend fun sendWeatherForecastRq(): WeatherResult {
        return withContext(Dispatchers.IO){
            val resp = provider.oneCall(lattitude = 19.4326296, longitude = -99.1331785, count = 6 )
            processHostResponse(resp)
        }
    }
    private fun processHostResponse(response: Response<OpenWeatherResponse>): WeatherResult {
        if (!response.isSuccessful){
            return WeatherResult(false,"Host Error: ${response.code()}")
        }
        return WeatherResult(true,null, response.body())
    }
}