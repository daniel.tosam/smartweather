package com.daniweb.smartweather.network.models

import OpenWeatherResponse

data class WeatherResult(val succeed: Boolean = false,
                         val msg: String?,
                         val response: OpenWeatherResponse? = null )
