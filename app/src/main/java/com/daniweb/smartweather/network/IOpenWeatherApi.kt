package com.daniweb.smartweather.network

import OpenWeatherResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface IOpenWeatherApi {

    @GET("onecall")
    suspend fun oneCall(@Query("lat") lattitude: Double,
                        @Query("lon") longitude: Double,
                        @Query("exclude", encoded = true) exclude: String = "minutely,hourly,alerts",
                        @Query("cnt") count: Int,
                        @Query("units") units: String = "metric",
                        @Query("appid") api_key: String = "df0862002c2c475c576d0ea0d35fc2ff"
    ) : Response<OpenWeatherResponse>
}