package com.daniweb.smartweather.ui.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.daniweb.smartweather.R
import com.daniweb.smartweather.Utils
import com.daniweb.smartweather.WeatherIconManager
import com.daniweb.smartweather.databinding.FragmentDetailBinding
import com.daniweb.smartweather.databinding.FragmentHomeBinding
import com.daniweb.smartweather.ui.home.HomeViewModel


class DetailFragment : Fragment() {
    private lateinit var binding: FragmentDetailBinding
    private lateinit var navController: NavController
    private lateinit var viewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentDetailBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(binding.root)
        viewModel = ViewModelProvider(this)[HomeViewModel::class.java]

        initViews()
    }

    private fun initViews() {
        binding.dateTv.text = arguments?.getInt("dt")?.let { Utils.getDate(it.toLong()) }
        binding.description.text = arguments?.getString("description")
        binding.forecastImg.setImageResource( WeatherIconManager(arguments?.getString("weather") ?: ""))
        binding.temperatureTv.text = arguments?.getDouble("day_temp")?.let { getString(R.string.celcius_temp,it) }
        binding.dayTemp.text = arguments?.getDouble("day_temp")?.let { getString(R.string.celcius_temp,it) }
        binding.morningTemp.text = arguments?.getDouble("morn_temp")?.let { getString(R.string.celcius_temp,it) }
        binding.eveningTemp.text = arguments?.getDouble("eve_temp")?.let { getString(R.string.celcius_temp,it) }
        binding.humidity.text = arguments?.getInt("humidity")?.toString()
        binding.windSpeed.text = arguments?.getDouble("wind_speed")?.toString()
        binding.rain.text = arguments?.getDouble("rain")?.let { (it*100).toString() }
        binding.btn.setOnClickListener {
            navController.popBackStack()
        }
    }

    private fun processUiState(){

    }


}