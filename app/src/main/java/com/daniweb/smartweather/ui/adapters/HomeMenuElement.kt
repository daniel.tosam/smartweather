package com.daniweb.smartweather.ui.adapters



data class HomeMenuElement(val day: String = "Mon",
                           val forecast_icon: Int,
                           val forecast: String,
                           val max_min_temp: String)