package com.daniweb.smartweather.ui.home

import OpenWeatherResponse
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.daniweb.smartweather.network.OpenWeatherService
import kotlinx.coroutines.launch

class HomeViewModel: ViewModel() {

    val uiState = MutableLiveData<Boolean>()
    val data = MutableLiveData<OpenWeatherResponse>()

    fun getWeatherForecast(){
        viewModelScope.launch {
            val result = OpenWeatherService.sendWeatherForecastRq()
            if (!result.succeed || result.response == null){
                uiState.postValue(false)
            } else {
                data.postValue(result.response!!)
            }
        }
    }

}