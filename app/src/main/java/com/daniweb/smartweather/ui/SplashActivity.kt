package com.daniweb.smartweather.ui

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity

import com.daniweb.smartweather.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder


class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if (isOnline(applicationContext)){
            Handler(Looper.getMainLooper()).postDelayed({
                val intent  = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            },5000)
        } else {
            MaterialAlertDialogBuilder(this)
                .setTitle(resources.getString(R.string.internet_label))
                .setMessage(resources.getString(R.string.internet_alert_desc))
                .setNegativeButton(resources.getString(R.string.decline_button_label)) { dialog, which ->
                    finish()
                    // Respond to negative button press
                }
                .setPositiveButton(resources.getString(R.string.accept_button_label)) { dialog, which ->
                    finish()
                    // Respond to positive button press
                }
                .show()

        }

    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                return true
            }
        }
        return false
    }
}