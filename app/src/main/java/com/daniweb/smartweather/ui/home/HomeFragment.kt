package com.daniweb.smartweather.ui.home

import OpenWeatherResponse
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider

import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.daniweb.smartweather.R
import com.daniweb.smartweather.Utils
import com.daniweb.smartweather.WeatherIconManager
import com.daniweb.smartweather.databinding.FragmentHomeBinding
import com.daniweb.smartweather.ui.adapters.HomeMenuElement
import com.daniweb.smartweather.ui.adapters.MenuAdapter


class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var navController: NavController
    private lateinit var viewModel: HomeViewModel
    private val adapter = MenuAdapter(emptyList())

    private val itemListener = object : MenuAdapter.ItemClickListener{
        override fun itemClick(position: Int) {
            onItemClicked(position)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater,container,false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(binding.root)
        viewModel = ViewModelProvider(this)[HomeViewModel::class.java]

        viewModel.uiState.observe(viewLifecycleOwner) {
            processUiState()
        }

        viewModel.data.observe(viewLifecycleOwner) {
            adapter.values = populateitems(it)
            adapter.notifyDataSetChanged()
        }
        initViews()
        viewModel.getWeatherForecast()
    }

    private fun processUiState() {

    }

    private fun initViews() {
        adapter.itemListener = itemListener
        binding.menuView.layoutManager = GridLayoutManager(context,1)
        binding.menuView.adapter = adapter
    }

    private fun populateitems(data: OpenWeatherResponse): List<HomeMenuElement>{
        val items = mutableListOf<HomeMenuElement>()
        data.daily.forEach {
            items.add(
                HomeMenuElement(
                    day = Utils.getDayOfWeek(it.dt.toLong()),
                    forecast_icon = WeatherIconManager(it.weather[0].main),
                    forecast = it.weather[0].main,
                    max_min_temp = getString(R.string.max_min_temp,it.temp.min, it.temp.max)
                )
            )
        }
        return items
    }

    private fun onItemClicked(item: Int){
        val selected = viewModel.data.value?.daily?.get(item)
        val bundle = bundleOf("dt" to (selected?.dt ?: 0),
            "day_temp" to (selected?.temp?.day ?: 0.0),
            "eve_temp" to (selected?.temp?.eve ?: 0.0),
            "morn_temp" to (selected?.temp?.morn ?: 0.0),
            "feels_like" to (selected?.feels_like?.day ?: 0.0),
            "humidity" to (selected?.humidity ?: 0),
            "humidity" to (selected?.humidity ?: 0),
            "wind_speed" to (selected?.wind_speed ?: 0.0),
            "rein" to (selected?.pop ?: 0.0),
            "weather" to (selected?.weather?.get(0)?.main ?: ""),
            "description" to (selected?.weather?.get(0)?.description ?: ""),
        )
        navController.navigate(R.id.action_homeFragment_to_detailFragment, bundle)
    }


}