package com.daniweb.smartweather.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.daniweb.smartweather.R

class MenuAdapter(
    var values: List<HomeMenuElement>,
    var itemListener: ItemClickListener? = null ): RecyclerView.Adapter<MenuAdapter.ItemHolder>() {

    class ItemHolder(val view: View,
                     private val itemListener: ItemClickListener? = null ): RecyclerView.ViewHolder(view){
        fun render (element: HomeMenuElement){
            view.findViewById<TextView>(R.id.day_tv).text = element.day
            view.findViewById<TextView>(R.id.forecast_tv).text = element.forecast
            view.findViewById<ImageView>(R.id.forecast_img).setImageResource(element.forecast_icon)
            view.findViewById<TextView>(R.id.max_min_tv).text = element.max_min_temp
        }

        init {
            if (itemListener != null) {
                view.setOnClickListener {
                    itemListener.itemClick(adapterPosition)
                }
            }
        }
    }

    interface ItemClickListener{
        fun itemClick(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ItemHolder(layoutInflater.inflate(R.layout.home_menu_element_view,parent,false),itemListener)
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) = holder.render(values[position])

    override fun getItemCount(): Int = values.size
}